//
//  StringExtensions.swift
//  Marvel X
//
//  Created by AntonioFrade on 24/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation
import CommonCrypto

public extension String {
    
    func MD5() -> String? {
        
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        if let d = self.data(using: .utf8) {
            _ = d.withUnsafeBytes { body in
                CC_MD5(body.baseAddress, CC_LONG(d.count), &digest)
            }
        }
        
        return (0..<length).reduce("") { $0 + String(format: "%02x", digest[$1]) }
    }
}
