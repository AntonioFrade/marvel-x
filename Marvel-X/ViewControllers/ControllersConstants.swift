//
//  ControllersConstants.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation
import UIKit

struct ControllersConstants {
    
    // MARK: - Vars & Lets
    static let mainStoryboard = "MainStoryboard"
    
    static let bgMainColor = UIColor.init(white: 0.95, alpha:1.0)
    static let textMainColor = UIColor.init(white: 0.1, alpha:1.0)
    
    static let characterCellIdentifier = "characterCellIdentifier"
    static let titleCharacterDetailCellIdentifier = "titleCharacterDetailCellIdentifier"
    static let comicsCharacterDetailCellIdentifier = "comicsCharacterDetailCellIdentifier"
}
