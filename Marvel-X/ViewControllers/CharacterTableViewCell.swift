//
//  CharacterTableViewCell.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = ControllersConstants.bgMainColor
        nameLabel!.textColor = ControllersConstants.textMainColor
        descriptionLabel!.textColor = ControllersConstants.textMainColor
        thumbnailImageView.backgroundColor = .clear
    }
    
    /*
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    */
}
