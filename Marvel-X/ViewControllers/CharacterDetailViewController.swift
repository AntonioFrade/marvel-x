//
//  CharacterDetailViewController.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import UIKit

class CharacterDetailViewController: UIViewController {
    
    // Header
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewHeightConst: NSLayoutConstraint!
    
    var headerViewHeightConstraint: NSLayoutConstraint!
    var headerViewMaxHeight: CGFloat = CGFloat.greatestFiniteMagnitude
    var headerViewMinHeight: CGFloat = 120.0
    
    @IBOutlet weak var characterImage: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifiers = [ControllersConstants.titleCharacterDetailCellIdentifier, ControllersConstants.comicsCharacterDetailCellIdentifier]
       
    
    var selectedCharacter:MarvelCharacter!
    var characterComics:[Comic] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = ControllersConstants.bgMainColor
        
        tableView.backgroundColor = ControllersConstants.bgMainColor
        tableView.separatorInset = .zero
        tableView.separatorColor = .gray
        tableView.keyboardDismissMode = .interactive
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        characterImage.contentMode = .scaleAspectFill
        characterImage.clipsToBounds = true
        
        //characterImage.image = image.resizeTopAlignedToFill(newWidth: imageView.frame.width)
        
        
        
        if let thumbnail = selectedCharacter.thumbnail {
            if let imageURL = thumbnail.fullURL {
                characterImage.kf.setImage (
                    with: imageURL,
                    placeholder: nil,
                    options: [ .transition(.fade(0.3)), .cacheOriginalImage])
            }
        }
        
        
        tableView.am.addPullToRefresh { [unowned self] in
            //append to your datasource
            self.loadCharacterComics { [unowned self] in
                self.tableView.am.pullToRefreshView?.stopRefreshing()
                self.tableView.reloadData()
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if characterComics.count == 0 {
            self.tableView.am.pullToRefreshView?.trigger()
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if headerViewMaxHeight == CGFloat.greatestFiniteMagnitude {
            
            headerViewMaxHeight = headerView.frame.size.height
            headerView.removeConstraint(headerViewHeightConst)
            view.removeConstraint(headerViewHeightConst)
            headerViewHeightConstraint = NSLayoutConstraint(item:headerView!,
                                                                   attribute:.height,
                                                                   relatedBy:.equal,
                                                                   toItem:nil,
                                                                   attribute:.notAnAttribute,
                                                                   multiplier: 1.0,
                                                                   constant: headerViewMaxHeight)
            
            headerView.addConstraint(headerViewHeightConstraint)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func loadCharacterComics(completion: @escaping () -> ()) {
        
        MarvelAPIManager.makeRequest(marvelRequest: MarvelCharacterComicRequest(characterId: selectedCharacter.id!)) { (response, error) in
            
            if let apiResponse = response {
                if let results = apiResponse.results {
                    self.characterComics = results
                }
            }
            completion()
        }
    }
}





extension CharacterDetailViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellIdentifiers.count
    }
    
    // number of rows in table view
    // Return the number of rows for the table.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return characterComics.count
        }
    }
    
    // Provide a cell object for each row.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = cellIdentifiers[indexPath.section]
        
        if cellIdentifier == ControllersConstants.titleCharacterDetailCellIdentifier {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TitleCharacterDetailTableViewCell
            
            cell.selectionStyle = .none
            cell.nameLabel!.text = ""
            cell.descriptionLabel!.text = ""
            if let name = selectedCharacter.name {
                cell.nameLabel!.text = name
            }
            if let marvelCharacterDescription = selectedCharacter.marvelCharacterDescription {
                cell.descriptionLabel!.text = marvelCharacterDescription
            }
            
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CharacterTableViewCell
            cell.selectionStyle = .none
            
            let comic = characterComics[indexPath.row]
            
            cell.nameLabel!.text = ""
            cell.descriptionLabel!.text = ""
            if let title = comic.title {
                cell.nameLabel!.text = title
            }
            if let comicDescription = comic.comicDescription {
                cell.descriptionLabel!.text = comicDescription
            }
            
            cell.thumbnailImageView.image = UIImage(named: "placeholder")
            
            if let thumbnail = comic.thumbnail {
                if let imageURL = thumbnail.fullURLVariant(thumbnailVariant: .portrait_uncanny) {
                    cell.thumbnailImageView.kf.setImage (
                        with: imageURL,
                        placeholder: nil,
                        options: [ .transition(.fade(0.3)), .cacheOriginalImage])
                }
            }
            
            return cell
        }
    }
}

extension CharacterDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


// MARK: - UIScrollViewDelegate
extension CharacterDetailViewController: UIScrollViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let scView = scrollView as? UITableView {
            if scView == tableView {
                
                let yOffset: CGFloat = scrollView.contentOffset.y
                let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - yOffset
                
                if newHeaderViewHeight > headerViewMaxHeight {
                    headerViewHeightConstraint.constant = headerViewMaxHeight
                    
                }
                else if newHeaderViewHeight < headerViewMinHeight {
                    headerViewHeightConstraint.constant = headerViewMinHeight
                }
                else {
                    
                    headerViewHeightConstraint.constant = newHeaderViewHeight
                    DispatchQueue.main.async {
                        //scrollView.contentOffset.y = 0 // block scroll view
                        //var scrollBounds = scrollView.bounds;
                        //scrollBounds.origin = CGPoint(x: 0, y: 0)
                        scrollView.bounds.origin = CGPoint(x: 0, y: 0)
                    }
                }
            }
        }
    }
}
