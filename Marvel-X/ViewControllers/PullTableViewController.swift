//
//  PullTableViewController.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import UIKit
import AMRefresher
import Kingfisher

class PullTableViewController: UITableViewController {
    
    var charactersList:[MarvelCharacter] = []
    var offsetContent = 0
    
    var searchText:String!
    var navigationTitleView:UIView!
    var titleLabel:UILabel!
    
    lazy var titleView: UIView = {
        
        titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.textColor = ControllersConstants.textMainColor
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        titleLabel.text = self.searchText
        
        let closeBtt = UIButton(type: .system)
        closeBtt.backgroundColor = .clear
        closeBtt.setImage(UIImage(named: "close"), for: .normal)
        closeBtt.addTarget(self, action: #selector(resetSearch), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            closeBtt.heightAnchor.constraint(equalToConstant: 20),
            closeBtt.widthAnchor.constraint(equalToConstant: 20)
        ])
        
        
        let stackView = UIStackView(arrangedSubviews: [closeBtt, titleLabel])
        stackView.spacing = 15
        stackView.alignment = .center
        stackView.axis = .horizontal
        
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        
        self.view.backgroundColor = ControllersConstants.bgMainColor
        self.navigationController?.navigationBar.tintColor = ControllersConstants.textMainColor
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: self, action: #selector(promptForCharacter))
        navigationItem.rightBarButtonItem!.image = UIImage(named: "search")
        self.title = "Marvel Characters"
        
        navigationTitleView = navigationItem.titleView
        
        tableView.backgroundColor = ControllersConstants.bgMainColor
        tableView.separatorInset = .zero
        tableView.separatorColor = .gray
        tableView.keyboardDismissMode = .interactive
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        
        tableView.am.addPullToRefresh { [unowned self] in
            //append to your datasource
            self.offsetContent = 0
            self.reloadCharactersList { [unowned self] in
                self.tableView.am.pullToRefreshView?.stopRefreshing()
                self.tableView.reloadData()
            }
        }
        
        //Adding Infinite Scrolling
        tableView.am.addInfiniteScrolling { [unowned self] in
            //append to your datasource
            
            self.loadMoreCharacters {
                self.tableView.am.infiniteScrollingView?.stopRefreshing()
                self.tableView.reloadData()
            }
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if charactersList.count == 0 {
            tableView.am.pullToRefreshView?.trigger()
        }
    }
    
    
    // MARK: - Update data source
    func reloadCharactersList(completion: @escaping () -> ()) {
        
        MarvelAPIManager.makeRequest(marvelRequest: MarvelCharacterRequest(nameStartsWith: searchText)) { (response, error) in
            
            if let apiResponse = response {
                if let results = apiResponse.results {
                    if let offset = apiResponse.offset {
                        self.offsetContent = offset + results.count
                    }
                    self.charactersList = results
                }
            }
            completion()
        }
    }
    
    
    func loadMoreCharacters(completion: @escaping () -> ()) {
        
        MarvelAPIManager.makeRequest(marvelRequest: MarvelCharacterRequest(nameStartsWith: searchText, offset: offsetContent)) { (response, error) in
            
            if let apiResponse = response {
                if let results = apiResponse.results {
                    if let offset = apiResponse.offset {
                        self.offsetContent = offset + results.count
                    }
                    self.charactersList.append(contentsOf:results)
                }
            }
            completion()
        }
    }
    
    func searchCharacter(textSearch:String) {
        
        searchText = textSearch
        tableView.am.pullToRefreshView?.trigger()
        navigationItem.titleView = self.titleView
        titleLabel.text = searchText
    }
    
    @objc func resetSearch() {
        searchText = nil
        navigationItem.titleView = navigationTitleView
        tableView.am.pullToRefreshView?.trigger()
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Action
    @objc func promptForCharacter() {
        
        let alertController = UIAlertController(title: "Look For Marvel Character", message: nil, preferredStyle: .alert)
        alertController.view.tintColor = ControllersConstants.textMainColor
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.textAlignment = .center
            textField.placeholder = "Character name"
            textField.textColor = ControllersConstants.textMainColor
        })
        
        let submitAction = UIAlertAction(title: "Go fot it", style: .default) { [unowned alertController, self] _ in
            let textField = alertController.textFields![0]
            // do something interesting with "answer" here
            self.searchCharacter(textSearch: textField.text!)
        }
        alertController.addAction(submitAction)
        present(alertController, animated: true)
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return charactersList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Fetch a cell of the appropriate type.
        let cellIdentifier = ControllersConstants.characterCellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CharacterTableViewCell
        
        
        // Configure the cell...
        // Configure the cell’s contents.
        
        let marvelCharacter = charactersList[indexPath.row]
        
        cell.nameLabel!.text = ""
        cell.descriptionLabel!.text = ""
        if let name = marvelCharacter.name {
            cell.nameLabel!.text = name
        }
        if let marvelCharacterDescription = marvelCharacter.marvelCharacterDescription {
            cell.descriptionLabel!.text = marvelCharacterDescription
        }
        
        cell.thumbnailImageView.image = UIImage(named: "placeholder")
        
        if let thumbnail = marvelCharacter.thumbnail {
            if let imageURL = thumbnail.fullURLVariant(thumbnailVariant: .portrait_uncanny) {
                cell.thumbnailImageView.kf.setImage (
                    with: imageURL,
                    placeholder: nil,
                    options: [ .transition(.fade(0.3)), .cacheOriginalImage])
            }
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let characterDetailViewController = UIStoryboard(name: ControllersConstants.mainStoryboard, bundle: nil)
            .instantiateViewController(withIdentifier:"characterDetailViewController_stID") as! CharacterDetailViewController
        
        characterDetailViewController.selectedCharacter = charactersList[indexPath.row]
        self.navigationController?.pushViewController(characterDetailViewController, animated: true)
    }
    
}
