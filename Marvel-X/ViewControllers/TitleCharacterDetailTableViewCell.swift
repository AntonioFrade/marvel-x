//
//  TitleCharacterDetailTableViewCell.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import UIKit

class TitleCharacterDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = ControllersConstants.bgMainColor
        nameLabel!.textColor = ControllersConstants.textMainColor
        descriptionLabel!.textColor = ControllersConstants.textMainColor
    }
    /*
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    */

}
