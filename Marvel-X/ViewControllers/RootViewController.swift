//
//  RootViewController.swift
//  Marvel-X
//
//  Created by Antonio Frade on 22/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = ControllersConstants.bgMainColor
        self.navigationController?.navigationBar.tintColor = ControllersConstants.textMainColor
        
        addBtts()
    }
    
    
    
    // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /*
    override func viewDidLayoutSubviews() {
    }
    */
    
    
   
    func addBtts () {
        
        let bttSize = CGSize(width: 160, height: 36)
        let spacebetweenBtts:CGFloat = 80.0
        
        let pullButtonFrame = CGRect(x: (self.view.frame.width/2) - (bttSize.width/2), y: self.view.frame.size.height*0.3, width: bttSize.width, height: bttSize.height)
        let pullButton = simpleBtt(frame: pullButtonFrame, title: "PullToRefresh TableView")
        pullButton.addTarget(self, action: #selector(showPullToRefreshTableView), for: .touchUpInside)
        
        var prefetchButtonFrame = pullButtonFrame
        prefetchButtonFrame.origin.y += spacebetweenBtts
        let prefetchButton = simpleBtt(frame: prefetchButtonFrame, title: "Prefetch TableView")
        prefetchButton.addTarget(self, action: #selector(showPreFetchTableView), for: .touchUpInside)
        
        self.view.addSubview(pullButton)
        self.view.addSubview(prefetchButton)
        
        /*
        var requestButtonFrame = pullButtonFrame
        requestButtonFrame.origin.y = self.view.frame.size.height*0.75
        let requestButton = simpleBtt(frame: requestButtonFrame, title: "Make Request")
        requestButton.addTarget(self, action: #selector(makeRequest), for: .touchUpInside)
        
        self.view.addSubview(requestButton)
        */
    }
    
    func simpleBtt(frame:CGRect, title:String) -> UIButton {
        
        let button = UIButton(type: .system)
        button.frame = frame
        button.backgroundColor = .clear
        button.setTitle(title, for: .normal)
        button.setTitleColor(ControllersConstants.textMainColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        
        button.layer.cornerRadius = frame.size.height/4.5
        button.layer.borderWidth = 1.0 / UIScreen.main.scale
        button.layer.borderColor = ControllersConstants.textMainColor.cgColor
        
        return button
    }
    
    
    @objc func showPullToRefreshTableView() {
        
        let pullTableViewController = UIStoryboard(name: ControllersConstants.mainStoryboard, bundle: nil)
            .instantiateViewController(withIdentifier:"pullTableViewController_stID") as! PullTableViewController
        
        self.navigationController?.pushViewController(pullTableViewController, animated: true)
    }
    
    @objc func showPreFetchTableView() {
        print("showPreFetchTableView")
    }
    
}

