//
//  MarvelAPIRequest.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation
import URLQueryItemEncoder

public protocol MarvelAPIRequest: Codable {
    
    associatedtype Response: Codable
    // Endpoint for request (the last part of the URL)
    var urlPath: String { get }
}


extension MarvelAPIRequest {
    
    func requestURL() -> URL? {
        guard let baseUrl = URL(string: self.urlPath, relativeTo: URL(string:MarvelAPIManager.baseURL())!) else {
            return nil
        }
        
        var urlComponents = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true)!
        urlComponents.queryItems = MarvelAPIManager.authURLComponents()
        
        do {
            let items = try URLQueryItemEncoder().encode(self)
            urlComponents.queryItems?.append(contentsOf: items)
        }
        catch {
            return nil
        }
        return urlComponents.url!
    }
}

