//
//  MarvelAPIManager.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation
import Alamofire


enum APIEnvironment:String {
    case publicAPI = "https://gateway.marvel.com/v1/public/"
}

class MarvelAPIManager {
    
    // MARK: - Vars & Lets
    static let networkEnviroment: APIEnvironment = .publicAPI
    
    class func baseURL() -> String {
        MarvelAPIManager.networkEnviroment.rawValue
    }
    
    
    class func authURLComponents() -> [URLQueryItem] {
        
        let timestamp = "\(Date().timeIntervalSince1970)"
        var urlComponents = URLComponents()
        urlComponents.queryItems = [
            URLQueryItem(name: "ts", value: timestamp),
            URLQueryItem(name: "apikey", value: MarvelAPIKeys.publicKey),
            URLQueryItem(name: "hash", value: "\(timestamp)\(MarvelAPIKeys.privateKey)\(MarvelAPIKeys.publicKey)".MD5())
        ]
        return urlComponents.queryItems!
    }
    
    
    class func makeRequest<T: MarvelAPIRequest>(marvelRequest:T, completion: @escaping(_  marvelResponse: DataContainer<T.Response>?, _ error: Error?) -> Void) {
        
        Alamofire.request(marvelRequest.requestURL()!,
                          method: .get,
                          parameters:nil,
                          encoding:URLEncoding.httpBody,
                          headers:nil)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let jsonData = response.data {
                        do {
                            let marvelResponse = try JSONDecoder().decode(MarvelResponse<T.Response>.self, from: jsonData)
                            completion(marvelResponse.dataContainer, nil)
                        }
                        catch let error {
                            //print("ERROR encoding MarvelResponse :: \(error.localizedDescription)")
                            completion(nil, error)
                        }
                    }
                    else {
                        completion(nil, nil)
                    }
                case let .failure(error):
                    if let jsonData = response.data {
                        do {
                            let marvelResponse = try JSONDecoder().decode(MarvelAPIError.self, from: jsonData)
                            completion(nil, marvelResponse)
                        }
                        catch let error {
                            completion(nil, error)
                        }
                    }
                    else {
                        completion(nil, error)
                    }
                }
        }
    }
}

