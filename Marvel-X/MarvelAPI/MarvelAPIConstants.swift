//
//  MarvelAPIConstants.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

struct MarvelAPIKeys {
    
    // MARK: - Vars & Lets
    static let publicKey = ""
    static let privateKey = ""
}
