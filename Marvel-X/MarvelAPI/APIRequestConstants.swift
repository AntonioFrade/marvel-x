//
//  APIRequestConstants.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

struct APIRequestConstants {
    
    static let defaultLimit = 20
}
