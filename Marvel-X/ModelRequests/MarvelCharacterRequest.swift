//
//  MarvelCharacterRequest.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation


// MARK: - MarvelCharacterRequest
struct MarvelCharacterRequest: MarvelAPIRequest {
    
    typealias Response = [MarvelCharacter]
    
    var urlPath: String {
        return "characters"
    }
    
    var name: String?
    var nameStartsWith: String?
    var modifiedSince: Date?
    var comics: Int?
    var series: Int?
    var events: Int?
    var stories: Int?
    var orderBy: String?
    var limit: Int?
    var offset: Int?
    
    
    init(name: String? = nil, nameStartsWith: String? = nil, modifiedSince: Date? = nil,
         comics: Int? = nil, series: Int? = nil,
         events: Int? = nil, stories: Int? = nil,
         orderBy: String? = nil, limit: Int? = APIRequestConstants.defaultLimit,
         offset: Int? = nil) {
        
        self.name = name
        self.nameStartsWith = nameStartsWith
        self.modifiedSince = modifiedSince
        
        self.comics = comics
        self.series = series
        self.events = events
        self.stories = stories
        
        self.orderBy = orderBy
        self.limit = limit
        self.offset = offset
    }
}
