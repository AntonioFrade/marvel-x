//
//  MarvelCharacterComicRequest.swift
//  Marvel X
//
//  Created by AntonioFrade on 26/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

// MARK: - MarvelComicRequest
struct MarvelCharacterComicRequest: MarvelAPIRequest {
    
    typealias Response = [Comic]
    
    public var urlPath: String {
        return "characters/\(characterId)/comics"
    }
    
    private let characterId: Int
    public init(characterId: Int) {
        self.characterId = characterId
    }
}
