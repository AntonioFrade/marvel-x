//
//  DataContainer.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

// MARK: - DataContainer
public struct DataContainer<Results: Codable>: Codable {
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    var results: Results?

    enum CodingKeys: String, CodingKey {
        case offset = "offset"
        case limit = "limit"
        case total = "total"
        case count = "count"
        case results = "results"
    }
}
