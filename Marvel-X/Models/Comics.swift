//
//  MarvelCharacterComics.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

// MARK: - ComicList
struct ComicList: Codable {
    var available: Int?
    var collectionURI: String?
    var items: [ComicSummary]?
    var returned: Int?

    enum CodingKeys: String, CodingKey {
        case available = "available"
        case collectionURI = "collectionURI"
        case items = "items"
        case returned = "returned"
    }
}

// MARK: - ComicSummary
struct ComicSummary: Codable {
    var resourceURI: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case resourceURI = "resourceURI"
        case name = "name"
    }
}


// MARK: - Comic
struct Comic: Codable {
    
    var id: Int?
    var digitalID: Int?
    var title: String?
    var issueNumber: Int?
    var comicDescription: String?
    var modified: String?
    var isbn: String?
    var upc: String?
    var diamondCode: String?
    var ean: String?
    var pageCount: Int?
    var resourceURI: String?
    var urls: [URLElement]?
    var thumbnail: Thumbnail?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case digitalID = "digitalId"
        case title = "title"
        case issueNumber = "issueNumber"
        case comicDescription = "description"
        case modified = "modified"
        case isbn = "isbn"
        case upc = "upc"
        case diamondCode = "diamondCode"
        case ean = "ean"
        case pageCount = "pageCount"
        case resourceURI = "resourceURI"
        case urls = "urls"
        case thumbnail = "thumbnail"
    }
}
