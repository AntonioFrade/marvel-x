//
//  Thumbnail.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

enum ThumbnailsVariants: String {
    
    //Portrait aspect ratio
    case  portrait_small        // 50x75px
    case  portrait_medium       // 100x150px
    case  portrait_xlarge        // 150x225px
    case  portrait_fantastic    // 168x252px
    case  portrait_uncanny      // 300x450px
    case  portrait_incredible   // 216x324px
    
    //Standard (square) aspect ratio
    case  standard_small        // 65x45px
    case  standard_medium       // 100x100px
    case  standard_large        // 140x140px
    case  standard_xlarge       // 200x200px
    case  standard_fantastic    // 250x250px
    case  standard_amazing        // 180x180px
    
    //Landscape aspect ratio
    case  landscape_small       // 120x90px
    case  landscape_medium        // 175x130px
    case  landscape_large        // 190x140px
    case  landscape_xlarge      // 270x200px
    case  landscape_amazing        // 250x156px
    case  landscape_incredible    // 464x261px
    
    //Full size images
    case  detail                // full image, constrained to 500px wide
    //case  full-size image     // no variant descriptor
}



// MARK: - Thumbnail
struct Thumbnail: Codable {
    
    var path: String?
    var thumbnailExtension: String?
    var fullURL: URL?

    enum CodingKeys: String, CodingKey {
        case path = "path"
        case thumbnailExtension = "extension"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let path = try container.decodeIfPresent(String.self, forKey: .path)
        let thumbnailExtension = try container.decodeIfPresent(String.self, forKey: .thumbnailExtension)
        
        self.init(path: path, thumbnailExtension: thumbnailExtension)
    }
    
    init(path: String?, thumbnailExtension: String?) {
        
        self.path = path
        self.thumbnailExtension = thumbnailExtension
        
        if let imagePath = self.path, let fileExtension = self.thumbnailExtension {
            self.fullURL = URL(string: "\(imagePath).\(fileExtension)")
        }
    }
}

extension Thumbnail {
    
    func fullURLVariant(thumbnailVariant: ThumbnailsVariants) -> URL? {
        
        if let imagePath = self.path, let fileExtension = self.thumbnailExtension {
            return URL(string: "\(imagePath)/\(thumbnailVariant.rawValue).\(fileExtension)")
        }
        return nil
    }
}
