//
//  URLElement.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

// MARK: - URLElement
struct URLElement: Codable {
    var type: String?
    var url: String?

    enum CodingKeys: String, CodingKey {
        case type = "type"
        case url = "url"
    }
}




