//
//  Events.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation


// MARK: - EventList
struct EventList: Codable {
    var available: Int?
    var collectionURI: String?
    var items: [EventSummary]?
    var returned: Int?

    enum CodingKeys: String, CodingKey {
        case available = "available"
        case collectionURI = "collectionURI"
        case items = "items"
        case returned = "returned"
    }
}

// MARK: - EventSummary
struct EventSummary: Codable {
    var resourceURI: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case resourceURI = "resourceURI"
        case name = "name"
    }
}
