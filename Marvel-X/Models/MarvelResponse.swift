//
//  MarvelResponse.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

// MARK: - MarvelResponse
public struct MarvelResponse<Response: Codable>: Codable {
    var code: Int?
    var status: String?
    var copyright: String?
    var attributionText: String?
    var attributionHTML: String?
    var etag: String?
    var dataContainer: DataContainer<Response>?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case status = "status"
        case copyright = "copyright"
        case attributionText = "attributionText"
        case attributionHTML = "attributionHTML"
        case etag = "etag"
        case dataContainer = "data"
    }
}
