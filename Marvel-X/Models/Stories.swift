//
//  Stories.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

// MARK: - StoryList
struct StoryList: Codable {
    var available: Int?
    var collectionURI: String?
    var items: [StorySummary]?
    var returned: Int?

    enum CodingKeys: String, CodingKey {
        case available = "available"
        case collectionURI = "collectionURI"
        case items = "items"
        case returned = "returned"
    }
}

// MARK: - StorySummary
struct StorySummary: Codable {
    var resourceURI: String?
    var name: String?
    var type: String?

    enum CodingKeys: String, CodingKey {
        case resourceURI = "resourceURI"
        case name = "name"
        case type = "type"
    }
}
