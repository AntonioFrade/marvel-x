//
//  MarvelCharacter.swift
//  Marvel X
//
//  Created by AntonioFrade on 25/04/2020.
//  Copyright © 2020 Antonio Frade. All rights reserved.
//

import Foundation

struct MarvelCharacter: Codable {
    
    var id: Int?
    var name: String?
    var marvelCharacterDescription: String?
    var modified: String?
    var thumbnail: Thumbnail?
    var resourceURI: String?
    
    var urls: [URLElement]?
    var comics: ComicList?
    var stories: StoryList?
    var events: EventList?
    var series: SeriesList?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case marvelCharacterDescription = "description"
        case modified = "modified"
        case thumbnail = "thumbnail"
        case resourceURI = "resourceURI"
        
        case urls = "urls"
        case comics = "comics"
        case stories = "stories"
        case events = "events"
        case series = "series"
    }
}

